//
//  ViewController.swift
//  udemyTimer
//
//  Created by Bryan on 2019-12-09.
//  Copyright © 2019 Bryan. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var timer: Int = 0
    var clock: Timer?
    
    let hoursInSec = 3600
    let minutesInSec = 60
   

    @IBOutlet weak var timeDisplay: UILabel!
    @IBOutlet weak var startButton: UIButton!
    @IBOutlet weak var stopButton: UIButton!
    @IBOutlet weak var ResetButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
       //This doesnt do anything but leaving it in for a reminder
    }
    
    @IBAction func addTime(sender: UIButton){
        
        let time = sender.currentTitle ?? "0"
        timer += Int(time) ?? 0
        timeDisplay.text = calculateDisplayTime()
    }
    
    @IBAction func commands(sender: UIButton){
        
        guard let button = sender.currentTitle else {return}
        
        if button == "start" {
            clock = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(countdown), userInfo: nil, repeats: true)
            startButton.isEnabled = false
            stopButton.isEnabled = true
        } else if button == "stop" {
            clock?.invalidate()
            startButton.isEnabled = true
            stopButton.isEnabled = false
        } else if button == "reset" {
            timer = 0
            timeDisplay.text = calculateDisplayTime()
            clock?.invalidate()
            startButton.isEnabled = true
            stopButton.isEnabled = false
        }
    }
    
    @objc func countdown(){
        timer -= 1
        
        if timer <= 0 {
            clock?.invalidate()
            timeDisplay.text = "Done!"
        } else {
            timeDisplay.text = calculateDisplayTime()
        }
    }

    private func calculateDisplayTime() -> String {
        var displayTime: String = ""
        var remainingTime: Int
        
        let hours = timer / hoursInSec
        remainingTime = timer % hoursInSec
        let minutes = remainingTime / minutesInSec
        let seconds = remainingTime % minutesInSec
        
        displayTime += (hours < 10) ? "0\(hours):" : "\(hours):"
        displayTime += (minutes < 10) ? "0\(minutes):" : "\(minutes):"
        displayTime += (seconds < 10) ? "0\(seconds)" : "\(seconds)"
                
        return displayTime
    }
}

